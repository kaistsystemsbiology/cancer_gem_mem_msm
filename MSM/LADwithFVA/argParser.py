import argparse

def argument_parser():
    parser = argparse.ArgumentParser()    

    parser.add_argument('-o', '--output_dir', required=True, help="Output directory")
    parser.add_argument('-dir', '--input_dir', required=True, help='Input model directory')
    parser.add_argument('-rw', '--reaction_weight_dir', required=True, help='reaction weight directory')
    parser.add_argument('-n', '--numOfJobs', default= 5, required=False, help="Number of cpu (int)")

    return parser
