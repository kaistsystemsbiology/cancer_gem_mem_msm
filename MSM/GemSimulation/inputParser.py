import argparse

def argument_parser():
    parser = argparse.ArgumentParser()    
    parser.add_argument('-o', '--output_dir', required=True, help="Output directory")
    parser.add_argument('-dir', '--input_dir', required=True, help='Input model directory')
    parser.add_argument('-exp', '--expression', required=False, help='Input expression file')
    parser.add_argument('-ladD', '--disable_lad', default=False, required=False, action='store_true', help="disable lad simulation")
    parser.add_argument('-fbaD', '--disable_fba', default=False, required=False, action='store_true', help="disable fba simulation")
    parser.add_argument('-spot_eflux2D', '--disable_spot_ef', default=False,required=False, action='store_true', help="disable spot and E_flux2 simulation")
    parser.add_argument('-rxnD', '--disable_reactome', default=False, required=False, action='store_true', help="disable parsing reactome")
    
    parser.add_argument('-n', '--num_core', default=10,required=False, help="number of cores for simulation")
    parser.add_argument('-rel', '--eFlux2_rel', default=0.99999, required=False, help="relaxation for E-flux2 simulation")
    
    return parser
