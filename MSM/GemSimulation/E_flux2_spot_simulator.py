from gurobipy import *
import cobra
import pandas as pd
import numpy as np
import copy


def expression2Const(model_inp, expression):
    
    model = copy.deepcopy(model_inp)
    bounds = {}
    for r in model.reactions:
        if r.id in expression:
            val = expression[r.id]
            if val ==0:
                bounds[r.id] = r.bounds
                continue
            else:
                if r.reversibility:
                    r.bounds = (-val, val)
                else:
                    r.bounds = (0.0, val)
                bounds[r.id] = r.bounds
        else:
            raise
    return model, bounds


class simulator:
    
    def __init__(self, model, r_weight):
        
        self.model = model
        self.r_weight = r_weight
        
        self.S = self.parseS()
        self.rxns = [x.id for x in self.model.reactions]
        self.S = self.S.fillna(0)
        self.S = self.S[self.rxns]
        
        self.no_gpr = []
        for r in self.model.reactions:
            if len(r.genes)==0:
                self.no_gpr.append(r.id)
        
    
    def parseS(self):
        S={}
        self.rev_rxn=[]
        self.irr_rxn=[]
        self.rev_rxn_bounds = {}
        for r in self.model.reactions:
            temp = {}
            for m in r.metabolites:
                temp[m.id]=r.get_coefficient(m.id)
            if r.reversibility:
                self.rev_rxn.append(r.id)
                self.rev_rxn_bounds[r.id]=r.bounds
            else:
                self.irr_rxn.append(r.id)
            S[r.id]=temp
        S = pd.DataFrame(S)
        
        return S
    
    def getGrowth(self):
        
        fx = Model('growth')
        fx.reset()
        
        V_r=[]
        for idx in range(len(self.rxns)):
            name=self.rxns[idx]
            V_r.append(fx.addVar(lb=self.newBounds[name][0], ub=self.newBounds[name][1], name=name))

        fx.update()
        
        S_matrix = self.S.values
           
        for idx in range(S_matrix.shape[0]):
            S_sub = S_matrix[idx,:]
            S_idx = np.nonzero(S_sub)[0]
            fx.addConstr(quicksum([S_sub[call]*V_r[call] for call in S_idx])==0)
            
        fx.update()    
            
        for idx, r in enumerate(self.rxns):
            if r == 'biomass_human':
                V_biomass = V_r[idx]
                fx.setObjective(V_biomass, GRB.MAXIMIZE)
        fx.optimize()
        
        return copy.deepcopy(V_biomass.x)
    
    def run_E_flux2(self, rel=0.99999):
        
        constModel, self.newBounds = expression2Const(self.model, self.r_weight)
        
        biomassFlux = self.getGrowth()
        
        fx = Model('Eflux2')
        fx.reset()
        
        V_r=[]
        
        for idx in range(len(self.rxns)):
            name=self.rxns[idx]
            if name == 'biomass_human':
                val = biomassFlux*rel
                V_r.append(fx.addVar(lb=val, ub=1000, name=name))

            else:
                V_r.append(fx.addVar(lb=self.newBounds[name][0], ub=self.newBounds[name][1], name=name))

        fx.update()
        S_matrix = self.S.values
        
        for idx in range(S_matrix.shape[0]):
            S_sub = S_matrix[idx,:]
            S_idx = np.nonzero(S_sub)[0]
            fx.addConstr(quicksum([S_sub[call]*V_r[call] for call in S_idx])==0)

        fx.update()
        fx.setObjective(quicksum([x*x for x in V_r]), GRB.MINIMIZE)
        fx.optimize()
        if fx.status == 2:
            sol = {x.VarName:x.x for x in V_r}

            return sol
        else:
            try:
                sub_sol = {x.VarName:x.x for x in V_r}
            except:
                sub_sol = {'error':"check"}
            return (fx.status, sub_sol)
    
    def run_spot(self, ham, qpconst_square=1):
        
        S_irr = self.S[self.irr_rxn]
        S_rev_f = self.S[self.rev_rxn]
        S_rev_b = self.S[self.rev_rxn]*-1
        
        if not ((abs(-1*self.S[self.rev_rxn])==abs(self.S[self.rev_rxn]))*1).sum().sum()/(self.S[self.rev_rxn].shape[0]*self.S[self.rev_rxn].shape[1])==1:
            raise
        if not (S_rev_f+S_rev_b).sum().sum()==0:
            raise
            
        S_irr_matrix = S_irr.values
        S_rev_f_matrix = S_rev_f.values
        S_rev_b_matrix = S_rev_b.values
        
        fx = Model('SPOT')
        fx.reset()
        
        
        
        V_irr = []
        V_rev_f = []
        V_rev_b = []
        for idx in range(len(self.irr_rxn)):
            name=self.irr_rxn[idx]
            if name == 'biomass_human':
                V_irr.append(fx.addVar(lb=0, ub=1000, name=self.irr_rxn[idx]))
            else:
                V_irr.append(fx.addVar(lb=0, ub=1000, name=self.irr_rxn[idx]))

        for idx, r_name in enumerate(self.rev_rxn):
            if r_name in ham:
                bounds = self.rev_rxn_bounds[r_name]
                V_rev_f.append(fx.addVar(lb=0, ub=bounds[1], name=self.rev_rxn[idx]+'_forward'))
            else:
                V_rev_f.append(fx.addVar(lb=0, ub=1000, name=self.rev_rxn[idx]+'_forward'))
        for idx, r_name in enumerate(self.rev_rxn):
            if r_name in ham:
                bounds = self.rev_rxn_bounds[r_name]
                V_rev_b.append(fx.addVar(lb=0, ub=abs(bounds[0]), name=self.rev_rxn[idx]+'_backward'))
            else:
                V_rev_b.append(fx.addVar(lb=0, ub=1000, name=self.rev_rxn[idx]+'_backward'))
        fx.update()

        for idx in range(S_irr_matrix.shape[0]):
            S_sub = S_irr_matrix[idx,:]
            S_sub2 = S_rev_f_matrix[idx,:]
            S_sub3 = S_rev_b_matrix[idx,:]

            S_idx = np.nonzero(S_sub)[0]
            S_idx2 = np.nonzero(S_sub2)[0]
            S_idx3 = np.nonzero(S_sub3)[0]
            fx.addConstr(quicksum([S_sub[call]*V_irr[call] for call in S_idx]+[S_sub2[call]*V_rev_f[call] for call in S_idx2]+[S_sub3[call]*V_rev_b[call] for call in S_idx3])==0)

        fx.update()
        
        V_univ = V_irr+V_rev_f+V_rev_b

        fx.addQConstr(quicksum([x*x for x in V_univ]), GRB.LESS_EQUAL, qpconst_square)
        fx.update()
        
        rxn_univ = self.irr_rxn+self.rev_rxn+self.rev_rxn
        fx.setObjective(quicksum([self.r_weight[item]*V_univ[idx] for idx, item in enumerate(rxn_univ)]), GRB.MAXIMIZE)


        fx.optimize()
        
        if fx.status == 2:
            F_irr = {x.VarName:x.x for x in V_irr}
            F_rev_f = {x.VarName.split('_forward')[0]:x.x for x in V_rev_f}
            F_rev_b = {x.VarName.split('_backward')[0]:x.x for x in V_rev_b}

            F_rev = {}
            for x in F_rev_f:
                F_rev[x] = F_rev_f[x]-F_rev_b[x]

            Flux = {}
            Flux.update(F_irr)
            Flux.update(F_rev)

            return Flux
        
        else:
            try:
                sub_sol = {x.VarName:x.x for x in V_r}
            except:
                sub_sol = {'error':"check"}
                
            return (fx.status, sub_sol)

        
        