import cobra

def getReactome(model):
    
    return {x.id:1 for x in model.reactions}



def runFBA(model):
    fba = model.optimize()
    fba_solution = fba.fluxes.to_dict()
    
    return fba_solution


def runpFBA(model):
    pfba_solution = cobra.flux_analysis.pfba(model)
    pfba_solution = pfba_solution.fluxes.to_dict()
    
    return pfba_solution