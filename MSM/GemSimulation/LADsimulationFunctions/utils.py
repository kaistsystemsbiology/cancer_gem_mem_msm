import pandas as pd
import pyparsing
import os

def split_expression_data_by_column(metabolic_expression_data_file, output_dir):
#     try:
#         shutil.rmtree(output_dir)
#     except:
#         pass

    try:
        os.mkdir(output_dir)
    except:
        pass

    metabolic_expression_df = pd.read_csv(metabolic_expression_data_file, index_col=0, header=0)
    print("%s samples in expression data!"%(len(metabolic_expression_df.columns)))
    for each_column in metabolic_expression_df.columns:
        metabolic_expression_df[each_column].to_csv(output_dir + '%s.csv' % (each_column), header=['Expression'], index_label=['ID'])
        
        
def convert_string_GPR_to_list_GPR(GPR_association):
    num = pyparsing.Word(pyparsing.alphanums)

    booleanop = pyparsing.oneOf('AND and OR or')
    expr = pyparsing.infixNotation(num,
                                   [(booleanop, 2, pyparsing.opAssoc.LEFT)])
    return expr.parseString(GPR_association)[0].asList()
