import pandas as pd
import cobra
import glob
import argparse
import copy
import logging
import os
import time
import warnings
from cobra.io import read_sbml_model
from GemSimulation.LADsimulationFunctions.generate_reaction_weight import *
from GemSimulation.LADsimulationFunctions.utils import *
from GemSimulation.LADsimulationFunctions import LAD, Simulator

def runLADsimulation(splited_expression_dir, cobra_model, generic_model, output_dir, getpra_file, scaling_factor = 100, use_getpra=False, reaction_weight_generation = True):
    start = time.time()
    logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)
    
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)
   
    omics_file = splited_expression_dir
    sample_id = os.path.basename(omics_file).split('.')[0] 
    print(omics_file, sample_id)
    
    if reaction_weight_generation:
        calculate_reaction_weight(output_dir, omics_file, getpra_file, use_getpra, cobra_model, generic_model) 
        print("Done reaction weight generation!") 
        
    else:
        pass
    
    print("Start LAD flux simulation!")
    model_reactions = [reaction.id for reaction in cobra_model.reactions]
    opt_flux_dist = {}
    with open(output_dir + '/Reaction_weight_%s.csv' %sample_id, 'r') as fp:
        fp.readline()
        for line in fp:
            sptlist = line.strip().split(',')
            rxn_id = sptlist[0].strip()
            flux = sptlist[1].strip()
            if rxn_id in model_reactions:
                opt_flux_dist[rxn_id] = float(flux)/scaling_factor


    flux_constraints = {}
    flux_constraints['biomass_human'] = [0.01, 1000.0]

    obj = LAD.LAD()
    obj.load_cobra_model(cobra_model)
    status, ObjVal, ReactionFlux = obj.run_LP_fitting(opt_flux=opt_flux_dist, flux_constraints=flux_constraints)
    
    if status == 2:
        print("LAD flux simulation found optimal solution!")
        predicted_flux = pd.DataFrame.from_dict(ReactionFlux, orient='index', columns=['Flux'])
        predicted_flux.to_csv(output_dir + '/Flux_prediction_%s.csv'%sample_id)

    else:
        print("Infeasible")
        

    logging.info(time.strftime("Elapsed time %H:%M:%S", time.gmtime(time.time() - start)))
    
if __name__ == '__main__':
    human1Model='/data/sangmi/Human-GEM/model/Human-GEM.xml'  
    getpra_file = '/data/sangmi/oncometabolites_prediction/oncometabolites/data/GeTPRA.txt'
    expression_dir = '/data/sangmi/fluxbenchmarking/sampleInputFile/expression/sampleExpression.csv'
    cobra_model_dir = '/data/sangmi/fluxbenchmarking/sampleInputFile/GEMs/DO222303.mat'
    output_dir = '../output'
    
    runLADsimulation(expression_dir, cobra_model_dir, human1Model, output_dir, scaling_factor=1000, use_getpra=False, reaction_weight_generation = False)