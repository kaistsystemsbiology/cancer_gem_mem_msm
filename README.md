# Evaluation of model extraction methods (MEMs) and model simulation methods (MSMs) #

### How to install
---
This code works on Linux, and has been tested on Ubuntu 16.04.

1. &nbsp;Clone the repository
```
git clone https://bitbucket.org/kaistsystemsbiology/cancer_gem_mem_msm.git
```
2. &nbsp;See README.md in each directory to implement rank-based tINIT or MSMs
    * Rank-based_tINIT directory: Two MATLAB functions available that implement rank-based tINIT
    * MSM directory: Python codes to implement five MSMs, including LAD, FBA, pFBA, SPOT, and E-Flux2 as well as iterativeLAD
---
All the MEMs were tested in MATLAB R2018b, and all the MSMs were implemented in Python 3.6 in this study.

### Reference
https://doi.org/10.1016/j.csbj.2022.06.027